-- Datenbanken: Projektarbeit (Noel Hödtke & Kolja Danowski)
-- Kundendatenbank inklusive Verbindung zu Bestell- und Zahlungssystem

-- DATABASE CREATION

DROP DATABASE IF EXISTS kverw;

CREATE DATABASE kverw;

USE kverw;

CREATE TABLE kontaktperson (
  id INT(64) NOT NULL  AUTO_INCREMENT,
  name VARCHAR(255),
  nachname VARCHAR(255),
  vorname VARCHAR(255),
  PRIMARY KEY(id)
);

CREATE TABLE kontakt (
  fk_kontaktperson INT(64),
  kontaktdatum VARCHAR(255),
  typ VARCHAR(16),
  FOREIGN KEY(fk_kontaktperson) REFERENCES kontaktperson(id)
);

CREATE TABLE kunde (
  id INT(64) NOT NULL AUTO_INCREMENT,
  fk_kontaktperson INT(64),
  name VARCHAR(255),
  adresse VARCHAR(255),
  seit DATE,
  PRIMARY KEY(id),
  FOREIGN KEY(fk_kontaktperson) REFERENCES kontaktperson(id)
);

CREATE TABLE bankdaten (
  fk_kunde INT(64),
  iban VARCHAR(32),
  bic VARCHAR(11),
  use_as_primary BOOLEAN,
  FOREIGN KEY(fk_kunde) REFERENCES kunde(id)
);

CREATE TABLE bestellung (
  id INT(64) NOT NULL AUTO_INCREMENT,
  fk_kunde INT(64),
  bestellt_am DATE,
  summe INT(64),
  bezahlt INT(64) DEFAULT 0,
  waehrung CHAR(3),
  letzte_zahlung DATETIME,
  PRIMARY KEY(id),
  FOREIGN KEY(fk_kunde) REFERENCES kunde(id)
);

CREATE TABLE zahlung (
  id INT(64) NOT NULL AUTO_INCREMENT,
  fk_bestellung INT(64),
  datum DATETIME,
  iban VARCHAR(32),
  bic CHAR(8),
  wert INT(64),
  PRIMARY KEY(id),
  FOREIGN KEY(fk_bestellung) REFERENCES bestellung(id)
);

-- END DATABASE CREATION

-- FUNCTIONS, PROCEDURES, TRIGGERS

DELIMITER //

-- FUNCTIONS

CREATE FUNCTION is_iban (iv_iban VARCHAR(32)) RETURNS BOOLEAN
BEGIN
  DECLARE v_iban_code VARCHAR(64) DEFAULT NULL;
  DECLARE v_iban_prefix VARCHAR(6);
  DECLARE EXIT HANDLER FOR sqlexception
    BEGIN
      RETURN FALSE;
    END;
  IF iv_iban IS NOT NULL THEN
    SET iv_iban = ucase(iv_iban);
    SET iv_iban = concat(substr(iv_iban, 5), substr(iv_iban, 1, 4));
    SET v_iban_code = replace_letters(iv_iban);
    IF convert(v_iban_code, DECIMAL(64)) MOD 97 = 1 THEN
      RETURN TRUE;
    END IF;
  END IF;
  RETURN FALSE;
END//

CREATE FUNCTION replace_letters(iv_iban_code VARCHAR(32)) RETURNS VARCHAR(64)
BEGIN
  DECLARE v_iban_code VARCHAR(64);
  DECLARE v_ascii_letters CHAR(26);
  DECLARE v_letter_to_replace CHAR(1);
  DECLARE v_replacement_digits INT;
  DECLARE i INT DEFAULT 0;
  SET v_ascii_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  SET v_iban_code = iv_iban_code;
  WHILE (i <= length(v_ascii_letters)) DO
    SET v_letter_to_replace = substr(v_ascii_letters, i, 1);
    SET v_replacement_digits = ascii(v_letter_to_replace) - 55;
    SET v_iban_code = replace(v_iban_code, v_letter_to_replace, v_replacement_digits);
    SET i = i + 1;
  END WHILE;
  RETURN v_iban_code;
END//

-- END FUNCTIONS

-- TRIGGERS

CREATE TRIGGER zahlung_validate_iban
  BEFORE INSERT ON zahlung
  FOR EACH ROW
BEGIN
  DECLARE v_is_iban BOOLEAN DEFAULT FALSE;
  SET v_is_iban = is_iban(new.iban);
  IF NOT v_is_iban = TRUE THEN
    SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = 'Field "iban" does not contain a valid IBAN';
  END IF;
END//

CREATE TRIGGER bankdaten_validate_iban
  BEFORE INSERT ON bankdaten
  FOR EACH ROW
BEGIN
  DECLARE v_is_iban BOOLEAN DEFAULT FALSE;
  SET v_is_iban = is_iban(new.iban);
  IF NOT v_is_iban = TRUE THEN
    SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = 'Field "iban" does not contain a valid IBAN';
  END IF;
END//

CREATE TRIGGER bezahlt_after_insert AFTER INSERT ON zahlung FOR EACH ROW
BEGIN
  CALL update_bezahlt(new.id);
END//

CREATE TRIGGER bezahlt_after_update AFTER UPDATE ON zahlung FOR EACH ROW
BEGIN
  CALL update_bezahlt(new.id);
END//

CREATE TRIGGER bezahlt_after_delete AFTER DELETE ON zahlung FOR EACH ROW
BEGIN
  CALL update_bezahlt(old.id);
END//

-- END TRIGGERS

-- PROCEDURES

CREATE PROCEDURE update_bezahlt (
  IN iv_bestellung INT(64)
)
BEGIN
  UPDATE bestellung SET bezahlt = (
    SELECT sum(wert) FROM zahlung
      WHERE fk_bestellung = iv_bestellung
  )
    WHERE id = iv_bestellung;
END//

CREATE PROCEDURE create_zahlung_simple (
  IN iv_bestellung INT(64),
  IN iv_wert INT(64)
)
BEGIN
  DECLARE v_iban VARCHAR(32);
  DECLARE v_bic CHAR(8);
  DECLARE v_kunde INT(64);
  DECLARE v_exists INT DEFAULT 0;
  DECLARE v_datum DATETIME;
  SET v_datum = now();
  SELECT count(id) FROM bestellung
    WHERE id = iv_bestellung
  INTO v_exists;
  IF v_exists = 0 THEN
    SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = 'Combination of Bestellungs-ID and Kunde does not exist';
  END IF;
  SELECT fk_kunde FROM bestellung
    WHERE id = iv_bestellung
  INTO v_kunde;
  SELECT iban, bic FROM bankdaten
    WHERE fk_kunde = v_kunde
    AND use_as_primary = TRUE
  INTO v_iban, v_bic;
  INSERT INTO zahlung(fk_bestellung, datum, iban, bic, wert)
    VALUES(iv_bestellung, v_datum, v_iban, v_bic, iv_wert);
  UPDATE bestellung SET letzte_zahlung = v_datum;
END//

-- END PROCEDURES

DELIMITER ;

-- END FUNCTIONS, PROCEDURES, TRIGGERS
