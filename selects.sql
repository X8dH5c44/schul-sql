USE kverw;

--Delete 2

DELETE * FROM bankdaten WHERE bankdaten.fk_kunde = (SELECT kontaktperson.id from kontaktperson WHERE kontaktperson.name = 'Pumuckel');

DELETE * FROM kunde WHERE kunde.fk_kunde = (SELECT kontaktperson.id from kontaktperson WHERE kontaktperson.name = 'Pumuckel');

--Update 3

UPDATE kontaktperson set kontaktperson.name = 'Bob der Baumeister' where kontaktperson.name = 'Pumuckel';

UPDATE kontaktperson set kontaktperson.name = 'Bob der Baumeister' where kontaktperson.name = 'Pumuckel'

UPDATE bankdaten set bankdaten.iban = 'DE47500105174656357473' where bankdaten.fk_kunde = (SELECT kunde.id from kunde where kunde.name = 'Friedrich GmbH');


--Verbundabfragen spezifisch 2

SELECT kunde.name, bestellung.bestellt_am, bestellung.letzte_zahlung, bestellung.bezahlt FROM kunde INNER JOIN bestellung on bestellung.fk_kunde = kunde.id WHERE kunde.fk_kontaktperson = (SELECT kontaktperson.id from kontaktperson WHERE kontaktperson.name = 'Pumuckel');

SELECT kunde.name, bankdaten.iban , bestellung.bestellt_am, bestellung.letzte_zahlung, bestellung.bezahlt FROM bestellung  INNER JOIN kunde on bestellung.fk_kunde = kunde.id INNER JOIN bankdaten on bankdaten.fk_kunde  = kunde.id WHERE    kunde.fk_kontaktperson = (SELECT kontaktperson.id from kontaktperson WHERE kontaktperson.name = 'Pumuckel');

--Verbundabfragen unspezifisch 3

SELECT kunde.name, bestellung.bestellt_am, bestellung.letzte_zahlung, bestellung.bezahlt FROM kunde INNER JOIN bestellung on bestellung.fk_kunde = kunde.id


SELECT kunde.name, bankdaten.iban , bestellung.bestellt_am, bestellung.letzte_zahlung, bestellung.bezahlt FROM bestellung  INNER JOIN kunde on bestellung.fk_kunde = kunde.id INNER JOIN bankdaten on bankdaten.fk_kunde  = kunde.id;


SELECT kunde.name, bankdaten.iban  FROM kunde  LEFT JOIN bankdaten on bankdaten.fk_kunde = kunde.id;


--Unscharfe Suche 1

Select * From kontaktperson WHERE kontaktperson.name like 'Pumu%';


--Scharfe Suche



--Filter



--Aggregation 3


SELECT MAX(summe) FROM `bestellung`;


SELECT MIN(summe) FROM `bestellung`;


SELECT AVG(summe) FROM `bestellung`;

--Total 14
