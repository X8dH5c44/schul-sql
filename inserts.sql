USE kverw;

INSERT INTO kontaktperson (name)
values (Pumuckel);


INSERT INTO kontaktperson (nachname, vorname)
VALUES (Friedrich, Tobias);

INSERT INTO kontakperson (nachname, vorname)
VALUES (Heins, Waltraut);


INSERT INTO kontakt (fk_kontaktperson, kontatkdatum, typ)
VALUES (1, 02161 8 23 83-66);

INSERT INTO kontakt (fk_kontaktperson, kontatkdatum, typ)
VALUES (2 , tobias.friedrich@kontaktmail.com);


INSERT INTO kontakt (fk_kontaktperson, kontatkdatum, typ)
VALUES (3, waltraut.heins@kontaktmail.com);


INSERT INTO kunde (fk_kontaktperson, name, adresse, seit)
VALUES (1, Pumuckel GmbH,42549 Velbert Pumuckelstraße 9, 2001-01-01);


INSERT INTO kunde (fk_kontaktperson, name, adresse, seit)
VALUES (2, Friedrich GmbH,42285 Wuppertal Friedrichstraße 5, 2009-06-06);


INSERT INTO kunde (fk_kontaktperson, name, adresse, seit)
VALUES (3, Heins GmbH,42551 Velbert Waltrautstraße , 2010-05-01);


INSERT INTO bankdaten (fk_kunde, iban, bic)
VALUES (1, DE95500105174874574696, 74574696);


INSERT INTO bankdaten (fk_kunde, iban, bic)
VALUES (2, DE90500105178551346885, 51346885);


INSERT INTO bankdaten (fk_kunde, iban, bic)
VALUES (3, DE96500105174827115779, 27115779);


INSERT INTO bestellung (fk_kunde, bestellt_am, summe,waehrung)
VALUES (1, 2001-05-01, 500, EUR);


INSERT INTO bestellung (fk_kunde, bestellt_am, summe, waehrung)
VALUES (2, 2010-05-05, 600, EUR);


INSERT INTO bestellung (fk_kunde, bestellt_am, summe, bezahlt, waehrung)
VALUES (3, 2011-06-06, 100, 100, EUR);

CALL  create_zahlung_simple (1, 1, 400);

CALL  create_zahlung_simple (2, 2, 600);

CALL  create_zahlung_simple (3, 3, 100);
